package todolist.todolist;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import todolist.todolist.model.ToDo;
import todolist.todolist.repository.ToDoRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
class TodolistApplicationTests {

	  @Test
	  void contextLoads() {
	  }
	
	  @Autowired
	  private ToDoRepository toDoRepository;

	  @Test
	  public void saveTest() {
	    ToDo toDo = new ToDo();
	    toDo.setCompleted(false);
	    toDo.setTitle("Kick");
	    ToDo savedEntity = toDoRepository.save(toDo);
	    assertThat(savedEntity.getCompleted()).isEqualTo(false);
	    assertThat(savedEntity.getTitle()).isEqualTo("Kick");
	  }
	  
	  @Test
	  public void findByIdTest() {
	    ToDo toDo = new ToDo();
	    toDo.setId((long) 1);
	    toDo.setCompleted(false);
	    toDo.setTitle("Kick");
	    ToDo savedEntity = toDoRepository.save(toDo);
	    ToDo foundEntity = toDoRepository.findById(savedEntity.getId()).orElse(null);
	    assertThat(foundEntity).isNotNull();
	    assertThat(foundEntity.getId()).isEqualTo(savedEntity.getId());
	    assertThat(foundEntity.getTitle()).isEqualTo("Kick");
	  }

	  @Test
	  public void updateTest() {
	    ToDo toDo = new ToDo();
	    toDo.setId((long) 1);
	    toDo.setTitle("Kick");
	    toDo.setCompleted(false);
	    ToDo savedEntity = toDoRepository.save(toDo);
	    savedEntity.setTitle("Chop");
	    ToDo updatedEntity = toDoRepository.save(savedEntity);
	    assertThat(updatedEntity.getId()).isEqualTo(savedEntity.getId());
	    assertThat(updatedEntity.getTitle()).isEqualTo("Chop");
	  }

	  @Test
	  public void deleteTest() {
	    ToDo toDo = new ToDo();
	    toDo.setId((long) 1);
	    toDo.setTitle("Chop");
	    toDo.setCompleted(false);
	    ToDo savedEntity = toDoRepository.save(toDo);
	    toDoRepository.delete(savedEntity);
	    ToDo deletedEntity = toDoRepository.findById(savedEntity.getId()).orElse(null);
	    assertThat(deletedEntity).isNull();
	  }
	  
}
