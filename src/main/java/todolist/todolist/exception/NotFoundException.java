package todolist.todolist.exception;

public class NotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public NotFoundException(String resourceName, String fieldName, Object fieldValue) {
		    super(String.format("%s not found with %s : '%s'", resourceName, fieldName, fieldValue));
		  }
}
