package todolist.todolist.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import todolist.todolist.model.ToDo;
import todolist.todolist.repository.ToDoRepository;

public class ToDoServiceImplementation{

	@Autowired
	private ToDoRepository toDoRepository;
	
	public Page<ToDo> getAllToDo(Pageable pageable) {
		
		Page<ToDo> allTodo = toDoRepository.findAll(pageable);
		return allTodo;
	}
	
}
