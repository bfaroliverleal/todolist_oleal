package todolist.todolist.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import todolist.todolist.exception.NotFoundException;
import todolist.todolist.model.ToDo;
import todolist.todolist.repository.ToDoRepository;

@RestController
@RequestMapping("/api/todos")
public class ToDoController {

	private final ToDoRepository toDoRepository;

    @Autowired
    public ToDoController(ToDoRepository toDoRepository) {
        this.toDoRepository = toDoRepository;
    }

    @PostMapping
    public ToDo createTodo(@RequestBody ToDo todo) {
    	todo.setCompleted(false);
        return toDoRepository.save(todo);
    }
    
    @GetMapping
    public ResponseEntity<Page<ToDo>> findAll(
        @RequestParam(value = "page", defaultValue = "0") int page,
        @RequestParam(value = "size", defaultValue = "10") int size,
        @RequestParam(value = "sort", defaultValue = "id") String[] sort) {

      Pageable pageable = PageRequest.of(page, size, Sort.by(sort));
      Page<ToDo> toDo = toDoRepository.findAll(pageable);
      return ResponseEntity.ok(toDo);
    }
    
    @GetMapping("/{id}")
    public ResponseEntity<ToDo> findById(@PathVariable Long id) {
      Optional<ToDo> entity = toDoRepository.findById(id);
      return entity.map(ResponseEntity::ok)
                  .orElseGet(() -> ResponseEntity.notFound().build());
    }
    
    @PutMapping("/{id}")
    public ResponseEntity<ToDo> updateTodo(@PathVariable Long id, @RequestBody ToDo updatedTodo) {
        if (!toDoRepository.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        
        updatedTodo.setId(id);
        toDoRepository.save(updatedTodo);
        return ResponseEntity.ok(updatedTodo);
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<ToDo> deleteEntity(@PathVariable Long id) {
    	toDoRepository.deleteById(id);
      return ResponseEntity.noContent().build();
    }


}
