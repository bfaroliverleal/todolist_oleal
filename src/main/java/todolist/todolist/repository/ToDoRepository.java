package todolist.todolist.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import todolist.todolist.model.ToDo;

public interface ToDoRepository extends JpaRepository<ToDo, Long>{

	Optional<ToDo> findById(Long id);
}
